var express = require("express");
var bodyParser = require("body-parser");
var app = express();
var port = (process.env.PORT || process.env.VCAP_APP_PORT || 8080);
var server = require("http").createServer(app);
var io = require("socket.io").listen(server);
server.listen(port, function(){
	console.log("server is running on localhost:8080\n*\n*\n*")
});


app.enable('trust proxy');
app.use(bodyParser.urlencoded({ extended: false}));
app.use(bodyParser.json());

//this is set to enable https
app.use(function (req, res, next) {
	if (req.secure) {
		next();
	} 
	else {
		res.redirect('https://' + req.headers.host + req.url);
	}	
});


app.use(express.static(__dirname + "/public"));


io.sockets.on('connection', function(socket){
  
  socket.on("connectScreen", function(){
    Screens.push(new screen(socket.id));
    console.log("openning a new loby with the id " + socket.id);
  });

  socket.on("connectController", function(screenId){
    var index = Screens.indexOf(screenId);
    if(index == -1){
	  	io.emit("error", parameter);
    } else{
      Screens.controllers.push(new controller(socket.id, screenId));
      console.log("new controller in loby " + screenId);
    }
  });
  
	//disconnect
	socket.on('disconnect', function(data){
    for(i=0; i<Screens.length; i++){
      if(Screens[i].id == socket.id){
        Screens.splice(i, 1);
        return;
      }
    }
    for(i=0; i<Screens.length; i++){
      for(j=0; j<Screens[i].controllers; j++){
        if(Screens[i].controllers[j].id == socket.id){
          Screens[i].controllers.splice(j, 1);
          return;
        }
      }
    }
    /* NON AUTHORISED DEVICE
      * if the socket wasn't a controller or a screen, it mean that a person is
      * trying to craft a connection to find security leak, so let's print his IP adress
    */ 

    console.log("UNAUTHORISED DEVICE: " + socket.conn.remoteAddress);
	});	

});

app.post("/screen", function(request, response){
  var object = request.body;
  

	response.end();
});

var Screens = [];

function screen(socket, controllerType){
  this.id = socket,
  this.controllerType = controllerType,
  this.controllers = []
}

function controller(socket, game){
  this.id = socket,
  this.game = game,
  this.controllerType
}